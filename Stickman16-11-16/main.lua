  mouse = {}
  boxleft = {}
  Menu = {}
  
  
function love.load()
  --spirtes images 
  
   --spirtes images 
  Stickman = love.graphics.newImage("sprites/Stickman.JPG")
  StickmanPosY = 200
  StickmanPosX = 200
  

-- for left and right movement
  boxleft.x = 0
  boxleft.y = 0
  
  
  
  
  -- platforms
  platform = love.graphics.newImage("sprites/platform.png")
  platformPosX = 0
  platformPosY = 250
  
  Stickmanground = StickmanPosY
  StickmanPosY_velocity = 0
  
  Stickmanjump = -300
  Stickmangravity = -500
 
  
  
  
  
  
  
  
  
   Menu.background= love.graphics.newImage("Asset/MenuBackground.jpg")
   Menu.backgroundQuad = love.graphics.newQuad(1,1,720/2,1280/2,720/2,1280/2)
  
   Menu.ButtonStart = love.graphics.newImage("Asset/MenuStart.jpg")
   Menu.ButtonStartHover = love.graphics.newImage("Asset/MenuStartHover.jpg")
   Menu.StartPosY = 100
  
   Menu.ButtonLoad = love.graphics.newImage("Asset/MenuLoad.jpg")
   Menu.ButtonLoadHover = love.graphics.newImage("Asset/MenuLoadHover.jpg")
   Menu.LoadPosY = 200
  
   Menu.ButtonOptions = love.graphics.newImage("Asset/MenuOptions.jpg")
   Menu.ButtonOptionsHover = love.graphics.newImage("Asset/MenuOptionsHover.jpg")
   Menu.OptionsPosY = 300
  
   Menu.ButtonExit = love.graphics.newImage("Asset/MenuExit.jpg")
   Menu.ButtonExitHover = love.graphics.newImage("Asset/MenuExitHover.jpg")
   Menu.ExitPosY = 400
   
   menu = true
   options = false
   loadSave = false
   
   gameScene = false
   
   
 
  
end

function love.update(dt)


if (gameScene == true) then
  
mouse.x, mouse.y = love.mouse.getPosition()
       
       
	if love.keyboard.isDown('space') then                  
 
       
		if StickmanPosY_velocity == 0 then
			StickmanPosY_velocity = Stickmanjump    
		end
	end
   if StickmanPosY_velocity ~= 0 then                                    
		StickmanPosY = StickmanPosY + StickmanPosY_velocity * dt               
		StickmanPosY_velocity = StickmanPosY_velocity -Stickmangravity * dt 
	end



end
end


function love.draw()
--draw

  --love.graphics.print("Mouse Coordinates: " .. mouse.x .. ", " .. mouse.y)
  game_screen()
end

function game_screen()

  if (gameScene == true)then
  
  love.graphics.draw(Stickman, StickmanPosX, StickmanPosY)
  love.graphics.draw(platform, platformPosX, platformPosY)

  
  --StickmanPosX = StickmanPosX + 1.5
  hitTest = CheckCollision(boxleft.x, boxleft.y, 200, 1280, mouse.x, mouse.y, 100, 100)
  if (hitTest) then
   StickmanPosX = StickmanPosX - 1.5
   
 else
   StickmanPosX = StickmanPosX + 1.5
  end
  
  hitTest = CheckCollision(platformPosX, platformPosY, 230, 50, StickmanPosX, StickmanPosY, 100, 100)
  if (hitTest) then
      StickmanPosY_velocity = 0
    	StickmanPosY = Stickmanground 
 else
   StickmanPosY = StickmanPosY + 1
   end
  
  
  
  end
  
  if (menu == true) then
  love.graphics.draw(Menu.background, Menu.backgroundQuad, 0, 0)
  love.graphics.draw(Menu.ButtonStart,90, Menu.StartPosY)
  love.graphics.draw( Menu.ButtonLoad, 90,Menu.LoadPosY)
  love.graphics.draw(Menu.ButtonOptions,90, Menu.OptionsPosY)
  love.graphics.draw(Menu.ButtonExit, 90, Menu.ExitPosY)
end 
 if (loadSave == true) then
   love.graphics.draw(Menu.background, Menu.backgroundQuad, 0, 0)
   
   love.graphics.draw(Menu.ButtonExit, 90, Menu.ExitPosY)
   
 end
  if (options == true) then
    love.graphics.draw(Menu.background, Menu.backgroundQuad, 0, 0)
    love.graphics.draw(Menu.ButtonExit, 90, Menu.ExitPosY)
  end
  

mouseX  = love.mouse.getX()
mouseY = love.mouse.getY()
checkStart = pointInRectangle(mouseX,mouseY,90,Menu.StartPosY,178,64)
checkLoad =  pointInRectangle(mouseX,mouseY,90,Menu.LoadPosY,178,64)
checkOptions = pointInRectangle(mouseX,mouseY,90,Menu.OptionsPosY,178,64)
checkExit = pointInRectangle(mouseX,mouseY,90,Menu.ExitPosY,178,64)
if (checkStart and menu == true)then
  love.graphics.draw(Menu.ButtonStartHover,90, Menu.StartPosY)
end
if (checkLoad and menu == true)then
  love.graphics.draw(Menu.ButtonLoadHover,90, Menu.LoadPosY)
end
if (checkOptions and menu == true)then
  love.graphics.draw(Menu.ButtonOptionsHover,90, Menu.OptionsPosY)
end
if (checkExit )then
  love.graphics.draw(Menu.ButtonExitHover,90, Menu.ExitPosY)
end

end
function Save()

end
function Physics ()

end
function love.mousepressed(x, y, button, istouch)
  if ( menu == true)then
    if (button == 1 and checkStart) then
      
      menu = false
      gameScene = true
     
    end
    if (button == 1 and checkLoad) then
      
      loadSave = true
      menu = false
    end
    if (button == 1 and checkOptions) then
      
      options = true
      menu = false
    end
    if (button == 1 and checkExit) then
      love.event.quit()
    end
  end
  if (loadSave == true)then
  
    if (button == 1 and checkExit) then
      menu = true
      loadSave = false
    end
  
end
if (options == true)then
  
    if (button == 1 and checkExit) then
      menu = true
      options = false
    end
  
  end
    
    
end
function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end
function pointInRectangle(pointx, pointy, rectx, recty, rectwidth, rectheight)
  
  wx = rectx + rectwidth
  hx = recty + rectheight
    return pointx > rectx and pointy > recty and pointx < wx and pointy < hx
end
